//
//  MenuItem.swift
//  MWallet
//
//  Created by Ali Merhie on 7/4/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import Foundation
import UIKit

class MenuItem {
    let title: String
    let image: UIImage
    let vcIdentifier: String
    
    init(title: String, image: UIImage, vcIdentifier: String) {
        self.title = title
        self.image = image
        self.vcIdentifier = vcIdentifier
    }
}
