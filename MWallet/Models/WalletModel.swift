//
//  WalletModel.swift
//  MWallet
//
//  Created by Ali Merhie on 6/28/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import Foundation
import ObjectMapper

class WalletModel: Mappable, Codable  {
    
    var id: Int?
    var user_id: Int?
    var chart_of_account_id: Int?
    var identifier: String?
    var max_credit: Decimal?
    var balance: String?
    var wallet_type_id: String?
    var created_at: String?
    var updated_at: String?
    var wallet_name: String?
    var is_active: Int?
    var suspended: Int?
    var suspended_till: String?
    var status: String?
    var wterminated: Int?

    
    required init?(map: Map) {
        id <- map["id"]
        user_id <- map["user_id"]
        chart_of_account_id <- map["chart_of_account_id"]
        identifier <- map["identifier"]
        max_credit <- map["max_credit"]
        balance <- map["balance"]
        wallet_type_id <- map["wallet_type_id"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        wallet_name <- map["wallet_name"]
        is_active <- map["is_active"]
        suspended <- map["suspended"]
        suspended_till <- map["suspended_till"]
        status <- map["status"]
        wterminated <- map["wterminated"]

    }
    
    func mapping(map: Map) {
        
    }
    
}
