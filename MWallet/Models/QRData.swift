//
//  QRData.swift
//  MWallet
//
//  Created by Ali Merhie on 7/9/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import Foundation

struct QRData {
    var codeString: String?
}
