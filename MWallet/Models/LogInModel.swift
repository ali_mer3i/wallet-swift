//
//  LogInModel.swift
//  MWallet
//
//  Created by Ali Merhie on 6/27/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import Foundation
import ObjectMapper

class LogInResponseModel: Mappable {
    
    var Token: String?
    var Name: String?
    var dob: String?
    var Mobile: String?

    
    required init?(map: Map) {
        Token <- map["api_token"]
        Name <- map["full_name"]
        dob <- map["dob"]
        Mobile <- map["mobile_number"]

    }
    init() {
        
    }
    
    func mapping(map: Map) {
        
    }
    
}
