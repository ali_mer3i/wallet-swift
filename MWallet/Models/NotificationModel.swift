//
//  TrasactionRequestModel.swift
//  MWallet
//
//  Created by Ali Merhie on 7/2/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import Foundation
import ObjectMapper

class NotificationModel: Mappable, Codable  {
    
    var full_name: String?
    var id: Int?
    var user_id: Int?
    var sender_id: Int?
    var seen: Int?
    var message: String?
    var notes: String?
    var amb_id: String?
    var redirect_to: String?
    var created_at: String?
    var updated_at: String?
    var type: String?
    
    
    required init?(map: Map) {
        full_name <- map["full_name"]
        id <- map["id"]
        user_id <- map["user_id"]
        sender_id <- map["sender_id"]
        seen <- map["seen"]
        message <- map["message"]
        notes <- map["notes"]
        amb_id <- map["amb_id"]
        redirect_to <- map["redirect_to"]
        created_at <- map["created_at"]
        updated_at <- map["updated_at"]
        type <- map["type"]
        
    }
    
    func mapping(map: Map) {
        
    }
    
}
