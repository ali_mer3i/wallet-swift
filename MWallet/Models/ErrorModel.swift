//
//  ErrorModel.swift
//  MWallet
//
//  Created by Ali Merhie on 6/27/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import Foundation
import ObjectMapper

class ErrorModel: Mappable {
    var error: String?
    var error_description: String?
    
    required init?(map: Map) {
        error <- map["error"]
        error_description <- map["error_description"]
    }
    
    func mapping(map: Map) {
        
    }
    
}
