//
//  ApiResponseAdapter.swift
//  Scarlet
//
//  Created by Ali Merhie on 5/3/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireObjectMapper
import ObjectMapper
import SwiftKeychainWrapper


class ApiResponseAdapter: RequestRetrier {
    
//    public typealias NetworkSuccessHandler = (AnyObject?) -> Void
//    public typealias NetworkFailureHandler = (HTTPURLResponse?, AnyObject?, NSError) -> Void
//
//    private typealias CachedTask = (HTTPURLResponse?, AnyObject?, NSError?) -> Void
//
//    public static var cachedTasks = Array<URLRequest>()
    public static var isRefreshing = false
    
    func should(_ manager: SessionManager, retry request: Request, with error: Error, completion: @escaping RequestRetryCompletion) {
        
        if let response = request.task?.response as? HTTPURLResponse{
            if response.statusCode == 401 {
               // requestsToRetry.append(completion)
                
//                getToken { (expires, _) in
//                    _ = SessionCountdownToken.sharedInstance.startCount(expirationTime: expires)
//                }
            } else {
                
                if request.retryCount == 3 { completion(false, 0.0 ); return}
                completion(true, 1.0)
                return
            }
        } else {
            completion(false, 0.0)
        }
    }
    
    
}

extension DataRequest {
    
    @discardableResult
    public func responseObject<T: BaseMappable>(queue: DispatchQueue? = nil, keyPath: String? = nil, mapToObject object: T? = nil, context: MapContext? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        return response(queue: queue, responseSerializer: DataRequest.ObjectMapperSerializer(keyPath, mapToObject: object, context: context), completionHandler: completionHandler)
    }
    
    @discardableResult
    public func responseObjectCustom<T: BaseMappable>(queue: DispatchQueue? = nil, keyPath: String? = nil, mapToObject object: T? = nil, context: MapContext? = nil, completionHandler: @escaping (DataResponse<T>) -> Void) -> Self {
        
        // let vc = AppDelegate.shared.window?.rootViewController
        
        responseJSON { response in
            
            self.handelStatusResponse(statusCode: response.response?.statusCode ?? 1,  request: response.request!)
        }
        let responseCustom = responseObject{ (response: DataResponse<T>) in
            completionHandler(response)
        }
        return responseCustom
    }
    
    @discardableResult
    public func responseArrayCustom<T: BaseMappable>(queue: DispatchQueue? = nil, keyPath: String? = nil, context: MapContext? = nil, completionHandler: @escaping (DataResponse<[T]>) -> Void) -> Self {
        
        responseJSON { response in
            self.handelStatusResponse(statusCode: response.response?.statusCode ?? 1, request: response.request!)
            
        }
        let responseCustom = responseArray{ (response: DataResponse<[T]>) in
            completionHandler(response)
        }
        return responseCustom
    }
    
    private func handelStatusResponse(statusCode: Int,  request: URLRequest){
        switch statusCode {
        case 200:
            print("case 200")
        case 400:
            print("case 400")
//            self.responseJSON { (responseJson) in
//                let res = responseJson.value
//            }
            self.responseObject{ (response: DataResponse<ErrorModel>) in
                let error = response.value
                Shared.displayAlert(title: error?.error ?? "InApp error: ApiResponderAdapter", message: error?.error_description ?? "Unknown error", cancelText: "Dismiss")
            }
        case 401:
            print("case 401")
            Shared.displayAlert(title: "Session Expired", message: "You will be logged out !", cancelText: "Dismiss")
            Shared.logOut()
        case 422:
            print("case 422")
            self.responseObject{ (response: DataResponse<ErrorModel>) in
                let error = response.value
                Shared.displayAlert(title: error?.error ?? "Exception", message: error?.error ?? "Unknown error", cancelText: "Dismiss")
            }
        default:
            print("case default: \( statusCode)")
        }
        //remove hud after request is done
        if Thread.isMainThread{
        //AppDelegate.shared.window!.rootViewController?.hideHUD()
          //  LoaderHelper.hideCustomLoader()
            //remove hud after request is done
            AppDelegate.shared.window!.rootViewController?.hideHUD()
            
        }
    }
    
//    private func refreshToken(){
//        ApiResponseAdapter.isRefreshing = true
//        let username = KeychainWrapper.standard.string(forKey: USERNAME_KEYCHAIN) ?? ""
//        let password = KeychainWrapper.standard.string(forKey: PASSWORD_KEYCHAIN) ?? ""
//        let logInData = LogInRequestModel(deviceId: UIDevice.current.identifierForVendor!.uuidString , appCerId: 1, checkingCredentials: true, code: 0 )
//        ApiServices.login(email: username, password: password , logInData: logInData, completion: { (logInResponse, statusCode) in
//            if statusCode == 200 {
//                let response = logInResponse!
//                if response.Success == true &&  response.Confirmed ?? false {
//              
//                    Shared.saveCredentials(logInresult: logInResponse!, username: username, password: password)
//                   // self.refreshIsDone()
//                }else {
//                    Shared.logOut()
//                }
//            }else {
//                Shared.logOut()
//            }
//        })
//    }
    
//    func refreshIsDone() {
//        ApiResponseAdapter.isRefreshing = false
//
//        Alamofire.SessionManager.default.session.getTasksWithCompletionHandler { (sessionDataTask, uploadData, downloadData) in
//            sessionDataTask.forEach { $0.resume() }
//            uploadData.forEach { $0.resume() }
//            downloadData.forEach { $0.resume() }
//        }

        //let cachedTaskCopy = ApiResponseAdapter.cachedTasks
        //ApiResponseAdapter.cachedTasks.removeAll()
        
//      do {
//       try cachedTaskCopy.map { try $0 }
//        } catch  {
//            Shared.displayAlert(title: "Error Refresh", message: "", cancelText: "Done")
//        }
        
 //   }

}
