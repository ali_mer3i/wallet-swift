//
//  ApiServices.swift
//  Scarlet
//
//  Created by Ali Merhie on 5/3/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//


import Foundation
import AlamofireObjectMapper
import Alamofire
import SwiftKeychainWrapper
import ObjectMapper

class ApiServices {
     var token: String
    
    init() {
        token = KeychainWrapper.standard.string(forKey: "Token") ?? "NoToken"
        SessionManager.default.adapter = ApiRequestAdapter(accessToken: token, prefix: API_URL)
        SessionManager.default.retrier = ApiResponseAdapter()
    }
    //MARK: Autharization Apis here:
    
    static func login(email: String, password: String, completion: @escaping (LogInResponseModel?, Int) -> Void) {
        let url = URL(string: API_LOGIN)!
        var params = [String : Any]()
        params["email"] = email
        params["password"] = password
        let req = request(url, method: .post, parameters: params)
        
        req.responseObjectCustom { (response: DataResponse<LogInResponseModel>) in
            
            completion(response.value, response.response!.statusCode)
        }
        
    }
    
    static func register(email: String, password: String, name: String, mobile: String, dob: String, completion: @escaping (LogInResponseModel?, Int) -> Void) {
        let url = URL(string: API_REGISTER)!
        var params = [String : Any]()
        params["email"] = email
        params["password"] = password
        params["full_name"] = name
        params["mobile_number"] = mobile
        params["dob"] = dob

        let req = request(url, method: .post, parameters: params)
        
        req.responseObjectCustom { (response: DataResponse<LogInResponseModel>) in
            
            completion(response.value, response.response!.statusCode)
        }
        
    }
    
    
    //MARK: Wallet Apis here:
    
    static func getAllWallets(completion: @escaping ([WalletModel]?, Int) -> Void) {
        let url = URL(string: API_GET_USER_WALLETS)!

        let req = request(url, method: .get)
        
        req.responseArrayCustom { (response: DataResponse<[WalletModel]>) in
            completion(response.value, response.response!.statusCode)
        }
    }
    
    static func createWallet(walletName: String, walletTypeId: Int, completion: @escaping (LogInResponseModel?, Int) -> Void) {
        let url = URL(string: API_GET_USER_WALLETS)!
        var params = [String : Any]()
        params["wallet_name"] = walletName
        params["wallet_type_id"] = walletTypeId
        let req = request(url, method: .post, parameters: params)
        
        req.responseObjectCustom { (response: DataResponse<LogInResponseModel>) in
            
            completion(response.value, response.response!.statusCode)
        }
        
    }
    
    static func transferAmmount(waleltID: String, amount: String, receiving: String, description: String, completion: @escaping (LogInResponseModel?, Int) -> Void) {
        let url = URL(string: API_TRANSFER_WALLET)!
        var params = [String : Any]()
        params["wallet_id"] = waleltID
        params["amount"] = amount
        params["receiving"] = receiving
        params["description"] = "IOS"

        let req = request(url, method: .post, parameters: params)
        
        req.responseObjectCustom { (response: DataResponse<LogInResponseModel>) in
            
            completion(response.value, response.response!.statusCode)
        }
        
    }
    
    //MARK: Transactions
    static func getNotifications(completion: @escaping ([NotificationModel]?, Int) -> Void) {
        let url = URL(string: API_GET_TRASANCTION_REQUESTS)!
        
        let req = request(url, method: .get)
        
        req.responseArrayCustom { (response: DataResponse<[NotificationModel]>) in
            completion(response.value, response.response!.statusCode)
        }
    }
    
    static func respondTrasactionRequest(isAccepted: Bool ,trasanctionID: String ,completion: @escaping ([NotificationModel]?, Int) -> Void) {
        var url = URL(string: API_REJECT_TRASANCTION + trasanctionID)!
        if isAccepted {
             url = URL(string: API_ACCEPT_TRASANCTION + trasanctionID)!
        }
        
        let req = request(url, method: .get)
        
        req.responseArrayCustom { (response: DataResponse<[NotificationModel]>) in
            completion(response.value, response.response!.statusCode)
        }
    }

    
}

