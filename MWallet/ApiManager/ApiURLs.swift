//
//  Constants.swift
//  Scarlet
//
//  Created by Ali Merhie on 4/30/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import Foundation

//MARK: URL here:
let HOST_URL = "http://monty-wallet.herokuapp.com"
let API_URL = HOST_URL + "/api/"

//MARK: AUTHARIZARION URL
let API_LOGIN = API_URL + "login"
let API_REGISTER = API_URL + "register"

//MARK: WALLET URL
let API_GET_USER_WALLETS = API_URL + "account/wallets"
let API_TRANSFER_WALLET = API_URL + "account/transfer"

//MARK: Trasaction URL
let API_GET_TRASANCTION_REQUESTS = API_URL + "account/notifications"
let API_ACCEPT_TRASANCTION = API_URL + "account/accept-transaction/"
let API_REJECT_TRASANCTION = API_URL + "account/decline-transaction/"

