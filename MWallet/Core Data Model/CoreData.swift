//
//  CoreData.swift
//  MWallet
//
//  Created by Ali Merhie on 7/8/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreData: UIView {
    static let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    //MARk: Country core
    public static func addfrquentContact(name: String, phone: String){
        
        //check if contact laready added
        let request : NSFetchRequest<ContactsFrequentCore> = ContactsFrequentCore.fetchRequest()
        request.predicate = NSPredicate(format: "phone CONTAINS[c] %@" , phone )
        do{
           let contacts =  try context.fetch(request)
            if contacts.count != 0 {
                var updatedContact = contacts[0]
                updatedContact.setValue(name, forKey: "name")
                updatedContact.setValue(phone, forKey: "phone")
                updatedContact.setValue(updatedContact.count + 1, forKey: "count")
                
            }else{
                let newContact = ContactsFrequentCore(context: self.context)
                newContact.name = name
                newContact.phone = phone
                newContact.count = 0
            }
            try self.context.save()

        } catch {
            print("Error fetching context , \(error)")
        }
    }
    
    public static func getFrequentContacts(with request : NSFetchRequest<ContactsFrequentCore> = ContactsFrequentCore.fetchRequest()) -> [ContactsFrequentCore] {
        var countries = [ContactsFrequentCore]()
        do{
            let sort = NSSortDescriptor(key: "count", ascending: false)
            request.sortDescriptors = [sort]
            countries =  try context.fetch(request)
            
      
        } catch {
            print("Error fetching context , \(error)")
        }
        return countries
    }
    
//    public static func deleteCountries() {
//
//        let countries =  self.getCountries()
//        for managedObject in countries
//        {
//            context.delete(managedObject)
//        }
//
//    }
    
}
