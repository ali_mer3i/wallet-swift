//
//  MoneyTransferMainViewController.swift
//  MWallet
//
//  Created by Ali Merhie on 6/17/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import UIKit

class MoneyTransferMainViewController: UIViewController {

    @IBOutlet weak var userDetailsView: UserDetailsView!
    @IBOutlet weak var liraMobileAccountBox: BoxView!
    @IBOutlet weak var CnicTransferBox: BoxView!
    @IBOutlet weak var BankAccoutBox: BoxView!
    @IBOutlet weak var favoritesCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // register collection cell
        let nib = UINib(nibName: "FavoritesCollectionViewCell", bundle: nil)
        favoritesCollectionView?.register(nib, forCellWithReuseIdentifier: "FavoritesCollectionViewCell")


//        userDetailsView.nameText.text = "Ali Merhi"
//        userDetailsView.numberText.text = "96170727050"
//        userDetailsView.balanceText.text = "Prepaid Balance"
//        userDetailsView.balanceValueText.text = "Rs. 1,755.00"
        
        //Lira Mobile Config
        liraMobileAccountBox.imageView.image = #imageLiteral(resourceName: "favourite.pdf")
        liraMobileAccountBox.titleText.text = "Lira Mobile Account"
        liraMobileAccountBox.descriptionText.text = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam"
        liraMobileAccountBox.currentViewController = self
        liraMobileAccountBox.nextVC = LiraAccount_VC
        
        //CnicConfig
        CnicTransferBox.imageView.image = #imageLiteral(resourceName: "favourite.pdf")
        CnicTransferBox.titleText.text = "CNIC Transfer"
        CnicTransferBox.descriptionText.text = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam"
        CnicTransferBox.currentViewController = self
        CnicTransferBox.nextVC = LiraAccount_VC
        
        //Bank Account Config
        BankAccoutBox.imageView.image = #imageLiteral(resourceName: "favourite.pdf")
        BankAccoutBox.titleText.text = "Bank Account"
        BankAccoutBox.descriptionText.text = "Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam"
        BankAccoutBox.currentViewController = self
        BankAccoutBox.nextVC = LiraAccount_VC
    }
    

}

extension MoneyTransferMainViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavoritesCollectionViewCell", for: indexPath)
        
        //TODO: Configure cell
        return cell
    }
}
extension MoneyTransferMainViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width/3, height: 60)
    }
}
