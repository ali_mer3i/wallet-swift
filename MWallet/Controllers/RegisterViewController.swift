//
//  RegisterViewController.swift
//  MWallet
//
//  Created by Ali Merhie on 6/28/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import UIKit

class RegisterViewController: UIViewController {

    @IBOutlet weak var nameText: BorderedTextField!
    @IBOutlet weak var emailText: BorderedTextField!
    @IBOutlet weak var passwordText: BorderedTextField!
    @IBOutlet weak var confirmPasswrodText: BorderedTextField!
    @IBOutlet weak var phoneNumberText: BorderedTextField!
    @IBOutlet weak var dobText: BorderedTextField!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var registerButton: FilledButton!
    @IBOutlet weak var subTitileText: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        nameText.placeholder = "Full Name"
        emailText.placeholder = "Email"
        passwordText.placeholder = "Password"
        confirmPasswrodText.placeholder = "Confirm Password"
        phoneNumberText.placeholder = "Phone Number"
        dobText.placeholder = "Date Of Birth"
        backButton.setTitle("Back to login", for: .normal)
        registerButton.setTitle("Register", for: .normal)
    }
    
    @IBAction func registerButtonClicked(_ sender: Any) {
        var errorExist = false
        
        if self.nameText.text == "" || self.emailText.text == "" || self.passwordText.text == "" || self.confirmPasswrodText.text == "" || self.phoneNumberText.text == "" || self.dobText.text == "" {
            errorExist = true
        }
        
        if errorExist{
            Shared.displayAlert(title: "Error", message: "All fields are required" , cancelText: "Done")
        }
        else{
            ApiServices.register(email: self.emailText.text!, password: self.passwordText.text!, name: self.nameText.text!, mobile: self.phoneNumberText.text!, dob: self.dobText.text!) { (userData, statusCode) in
                if statusCode == 200 {
                    Shared.logIn(logInresult: userData!)
//                    ApiServices.createWallet(walletName: self.phoneNumberText.text!, walletTypeId: WALLET_TYPES_ID.Normal.rawValue) { (response, statusCode) in
//                        if statusCode == 200 {
//                        }
//                }
                
            }
        }
    }
    }
    
    @IBAction func backClicked(_ sender: Any) {
        Shared.navigateWithNavigationBar(viewController: LogIn_VC)
    }
}
