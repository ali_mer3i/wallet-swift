//
//  BillPaymentsViewController.swift
//  MWallet
//
//  Created by Ali Merhie on 7/4/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import UIKit

class BillPaymentsViewController: UIViewController {
    let menuItems = [MenuItem(title: "Electricity", image:#imageLiteral(resourceName: "light-bulb.pdf") , vcIdentifier: ""),
                     MenuItem(title: "Telephone", image: #imageLiteral(resourceName: "call.pdf"), vcIdentifier: "CalendarViewController"),
                     MenuItem(title: "Gas", image: #imageLiteral(resourceName: "fuel-station.pdf"), vcIdentifier: "PatientsViewController"),
                     MenuItem(title: "Internet", image:#imageLiteral(resourceName: "internet.pdf") , vcIdentifier: "StaffViewController"),
                     MenuItem(title: "Water", image: #imageLiteral(resourceName: "water.pdf"), vcIdentifier: "ServiceViewController"),
                     MenuItem(title: "Solar", image:#imageLiteral(resourceName: "solar-energy.pdf") , vcIdentifier: "MetaViewController"),
                     MenuItem(title: "Government Fees", image: #imageLiteral(resourceName: "government.pdf"), vcIdentifier: "CurrencyViewController")
    ]
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Bill Payment"
        // Do any additional setup after loading the view.
    }
    



}
extension BillPaymentsViewController:  UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = menuItems[indexPath.row]
        let  cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        cell?.imageView?.image = item.image
        cell?.textLabel?.text = item.title
        return cell!
    }
    
}
