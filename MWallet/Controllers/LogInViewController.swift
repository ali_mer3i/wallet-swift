//
//  LogInViewController.swift
//  MWallet
//
//  Created by Ali Merhie on 6/14/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import UIKit
import MBProgressHUD

class LogInViewController: UIViewController {
    
    let apiservices: ApiServices = ApiServices()

    @IBOutlet weak var mobileNumberText: BorderedTextField!
    @IBOutlet weak var pinText: BorderedTextField!
    @IBOutlet weak var logInButton: FilledButton!
    @IBOutlet weak var noAccountLabel: UILabel!
    @IBOutlet weak var registerButton: BorderedButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mobileNumberText.placeholder = "Email"
        pinText.placeholder = "Password"
        logInButton.setTitle("Login", for: .normal)
        noAccountLabel.text = "Don't have an account ?"
        registerButton.setTitle("Create New Account", for: .normal)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func logInClicked(_ sender: Any) {
        
        var errorExist = false
        
        if self.mobileNumberText.text == "" || self.pinText.text == "" {
            errorExist = true
        }
        
        if errorExist{
            Shared.displayAlert(title: "Error", message: "All fields are required" , cancelText: "Done")
        }
        else{
            ApiServices.login(email: self.mobileNumberText.text!, password: self.pinText.text!) { (userData, statusCode) in
                if statusCode == 200 {
                    Shared.logIn(logInresult: userData!)
                }
                
            }
        }
    }
    
    @IBAction func moveToRegisterClicked(_ sender: Any) {
        Shared.navigateWithNavigationBar(viewController: Register_VC)
    }
    
}
