//
//  NotificationViewController.swift
//  MWallet
//
//  Created by Ali Merhie on 7/2/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import UIKit

class NotificationViewController: UIViewController {

    let apiservices: ApiServices = ApiServices()
    var refreshControl: UIRefreshControl!
    var notifications: [NotificationModel] = []
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        tableView.addSubview(refreshControl) 
        getNotifications()

        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 600
        tableView.register(UINib(nibName: "TrasanctionRequestTableViewCell", bundle: nil), forCellReuseIdentifier: "TrasanctionRequestTableViewCell")
        
    }
    
    private func getNotifications() {
        ApiServices.getNotifications { (NotificationsResponse, statusCode) in
            if statusCode == 200 {
                if let notificationsRes = NotificationsResponse {
                    self.notifications = notificationsRes
                    self.tableView.reloadData()
                    self.refreshControl.endRefreshing()

                }
            }
        }
    }
    @objc func refresh(_ sender: Any) {
        getNotifications()
        
    }

    
}
extension NotificationViewController:  UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notifications.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let currentNotification = notifications[indexPath.row]
        let  cell = tableView.dequeueReusableCell(withIdentifier: "TrasanctionRequestTableViewCell") as! TrasanctionRequestTableViewCell
        cell.delegate = self
        cell.trasanctionID = currentNotification.amb_id ?? ""
        cell.nameLabel.text = "\(currentNotification.full_name!)\(currentNotification.message!)"
        cell.amountLabel.text = ""
        
        if currentNotification.type == NOTIFICATON_TYPES.request.rawValue && currentNotification.seen == 1 {
            cell.buttonsView.isHidden = true
        }
        return cell
    }
    
}

extension NotificationViewController: TrasactionCellDelegate {
    func responseButtonClicked(isAccepted: Bool, trasanctionID: String) {
        ApiServices.respondTrasactionRequest(isAccepted: isAccepted, trasanctionID: trasanctionID) { (response, statusCode) in
            if statusCode == 200 {
                self.getNotifications()
                var message = "Trasanction rejected"
                if isAccepted {
                    message = "Trasanction accepted"
                }
                
                Shared.displayAlert(title: "Success", message: message, cancelText: "Done")
            }
        }
    }
    
    
}
