//
//  HomeViewController.swift
//  MWallet
//
//  Created by Ali Merhie on 6/17/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import UIKit
import MSPeekCollectionViewDelegateImplementation

class HomeViewController: UIViewController {
    var delegate: MSPeekCollectionViewDelegateImplementation!
    var advertiseImages: [String] = []
    enum SelectedButtonTag: Int {
        case Bundels
        case MoneyTransfer
        case BillPayment
    }

    @IBOutlet weak var pageControl: UIPageControl!
    @IBOutlet weak var imagesCollectionView: UICollectionView!
    @IBOutlet weak var advertiseCollectioView: UICollectionView!
    @IBOutlet weak var bundelsButton: UIButton!
    @IBOutlet weak var trasnferButton: UIButton!
    @IBOutlet weak var billButton: UIButton!
    @IBOutlet weak var paymentsButton: UIButton!
    @IBOutlet weak var uberButton: UIButton!
    @IBOutlet weak var discountsButton: UIButton!
    @IBOutlet weak var insuranceButton: UIButton!
    @IBOutlet weak var ticketsButton: UIButton!
    @IBOutlet weak var cashButton: UIButton!
    @IBOutlet weak var favoritesButton: UIButton!
    @IBOutlet weak var ViewAllButton: UIButton!
    @IBOutlet weak var remittanceButton: UIButton!
    
        override func viewDidAppear(_ animated: Bool) {
            super.viewDidAppear(true)
            //userDataView.loadData()
        }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "HOME"
        
        advertiseImages = ["advertise1", "advertise2", "advertise3"]
        
        imagesCollectionView.configureForPeekingDelegate()
        delegate = MSPeekCollectionViewDelegateImplementation(cellSpacing: 5, cellPeekWidth: 10)
        imagesCollectionView.delegate = delegate
        
        pageControl.pageIndicatorTintColor = UIColor(named: "avocadoTrans") 
        pageControl.currentPageIndicatorTintColor = UIColor(named: "avocado")
        
        // register collection cell
        let nib = UINib(nibName: "ImageCollectionViewCell", bundle: nil)
        advertiseCollectioView?.register(nib, forCellWithReuseIdentifier: "ImageCollectionViewCell")

        bundelsButton.setTitle("Easyload/    Bundels", for: .normal)
        trasnferButton.setTitle("Money Transfer", for: .normal)
        billButton.setTitle("Bill Payment", for: .normal)
        paymentsButton.setTitle("Payments", for: .normal)
        uberButton.setTitle("Uber TopUp", for: .normal)
        discountsButton.setTitle("Discounts", for: .normal)
        insuranceButton.setTitle("Insurance", for: .normal)
        ticketsButton.setTitle("Tickets", for: .normal)
        cashButton.setTitle("Cash In/Out", for: .normal)
        favoritesButton.setTitle("Favorites", for: .normal)
        ViewAllButton.setTitle("View All", for: .normal)
        remittanceButton.setTitle("Remittance", for: .normal)

//        bundelsText.text = "Easyload/Bundels"
//        transferText.text = "Money Transfer"
//        billText.text = "Bill Payment"
//        paymentsText.text = "Payments"
//        discountText.text = "Discounts"
//        uberText.text = "Uber TopUp"
//        insuranceText.text = "Insurance"
//        ticketsText.text = "Tickets"
//        remittanceText.text = "Remittance"
//        cashText.text = "Cash In/Out"
//        viewtext.text = "View All"
//        favoriteText.text = "Favorites"
        
//        userDetailsView.nameText.text = UserDefaults.standard.string(forKey: NAME_DEFUATLTS)
//        userDetailsView.numberText.text =  UserDefaults.standard.string(forKey: MOBILE_DEFUATLTS)
//        userDetailsView.balanceText.text = "Current Balance"
//        userDetailsView.balanceValueText.text =  UserDefaults.standard.string(forKey: CURRENT_BALANCE_DEFUATLTS)


        // Do any additional setup after loading the view.
    }

    @IBAction func itemClicked(_ sender: UIButton) {
        var selectedVC = MoneyTransferMain_VC
        
        switch sender.tag {
            
        case SelectedButtonTag.Bundels.rawValue:
            selectedVC = EasyLoad_VC
        case SelectedButtonTag.BillPayment.rawValue:
            selectedVC = BillPayment_VC
        default:
            selectedVC = MoneyTransferMain_VC

        }
        Shared.pushVC(rootView: self, viewController: selectedVC)
    }
}

extension HomeViewController: UICollectionViewDataSource, UICollectionViewDelegate {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let count = advertiseImages.count
        pageControl?.numberOfPages = count
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let image = advertiseImages[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionViewCell", for: indexPath) as! ImageCollectionViewCell
        //cell.contentView.backgroundColor = UIColor.red
        cell.imageView.image = UIImage(named: image)

        //TODO: Configure cell
        return cell
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let pageNumber = round(scrollView.contentOffset.x / scrollView.frame.size.width)
        pageControl.currentPage = Int(pageNumber)
    }
    
}
