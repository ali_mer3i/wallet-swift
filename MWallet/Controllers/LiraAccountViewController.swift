//
//  LiraAccountViewController.swift
//  MWallet
//
//  Created by Ali Merhie on 6/20/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import UIKit
import ContactsUI

class LiraAccountViewController: UIViewController, CNContactPickerDelegate  {
    let apiservices: ApiServices = ApiServices()
    var frequentContacts: [ContactsFrequentCore] = []
    var scannedPhoneNumber: String?
    
    @IBOutlet weak var AmountView: LinedTextFieldView!
    @IBOutlet weak var phoneView: LinedTextFieldView!
    @IBOutlet weak var favoritesCollectionView: UICollectionView!
    @IBOutlet weak var sendButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // register collection cell
        let nib = UINib(nibName: "FavoritesCollectionViewCell", bundle: nil)
        favoritesCollectionView?.register(nib, forCellWithReuseIdentifier: "FavoritesCollectionViewCell")
        sendButton.setTitle("Send", for: .normal)
        AmountView.textField.keyboardType = .numberPad
        AmountView.titleLabel.text = "Amount ($)"
        phoneView.titleLabel.text = "Phone Number"
        // Do any additional setup after loading the view.
        if let number = scannedPhoneNumber {
            phoneView.textField.text = number
        }
        updateFrequentContacts()
    }
    
    @IBAction func openContacts(_ sender: Any) {
        let contacVC = CNContactPickerViewController()
        contacVC.delegate = self
        self.present(contacVC, animated: true, completion: nil)
    }
    
    @IBAction func sendButtonClicked(_ sender: Any) {
        ApiServices.transferAmmount(waleltID: UserDefaults.standard.string(forKey: MAIN_WALLET_ID_DEFUATLTS) ?? "", amount: AmountView.textField.text!, receiving: phoneView.textField.text!, description: "") { (response, statusCode) in
            if statusCode == 200 {
                Shared.displayAlert(title: "Success", message: "Transaction is completed", cancelText: "Done")
            }
        }
    }
    
    func updateFrequentContacts(){
       frequentContacts = CoreData.getFrequentContacts()
        favoritesCollectionView.reloadData()
    }
    
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
        print(contact.phoneNumbers)
        let selectedContact = contact
        let numbers = contact.phoneNumbers.first
        let phoneNumber = "\((numbers?.value)?.stringValue.replacingOccurrences(of: " ", with: "") ?? "")"
        self.phoneView.textField.text = phoneNumber
        //save to Core Data
        CoreData.addfrquentContact(name: selectedContact.givenName + " " + selectedContact.familyName , phone: phoneNumber)

    }
    
    func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension LiraAccountViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return frequentContacts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let contact = frequentContacts[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavoritesCollectionViewCell", for: indexPath) as! FavoritesCollectionViewCell
        cell.button.setTitle(contact.name, for: .normal)
        cell.contact = contact
        cell.delegate = self
        
        //TODO: Configure cell
        return cell
    }
}

extension LiraAccountViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width/3, height: 60)
    }
}

extension LiraAccountViewController: FavoriteButtonCellDelegate {
    func FavoriteContactSelected(contact: ContactsFrequentCore) {
        self.phoneView.textField.text = contact.phone

    }
    
    
}
