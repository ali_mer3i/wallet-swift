//
//  EasyLoadViewController.swift
//  MWallet
//
//  Created by Ali Merhie on 6/21/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import UIKit

class EasyLoadViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var operatorsCollectionView: UICollectionView!
    @IBOutlet weak var typesCollectionView: UICollectionView!
    var selectedTypeCell : SelectionButtonCollectioViewCell = SelectionButtonCollectioViewCell()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Easyload/Bundels"
        let nib = UINib(nibName: "FavoritesCollectionViewCell", bundle: nil)
        operatorsCollectionView?.register(nib, forCellWithReuseIdentifier: "FavoritesCollectionViewCell")
        
        let nib1 = UINib(nibName: "SelectionButtonCollectioViewCell", bundle: nil)
        typesCollectionView?.register(nib1, forCellWithReuseIdentifier: "SelectionButtonCollectioViewCell")
        
        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 600
        tableView.register(UINib(nibName: "BundelsTableViewCell", bundle: nil), forCellReuseIdentifier: "BundelsTableViewCell")
        
    }

}

extension EasyLoadViewController:  UITableViewDataSource, UITableViewDelegate {
    
    // MARK: - UITableViewDataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let  cell = tableView.dequeueReusableCell(withIdentifier: "BundelsTableViewCell") as! BundelsTableViewCell
        cell.nameLabel.text = "4G Weekly Super"
        cell.validityLabel.text = "Valid for 7 days"
        cell.title1Label.text = "Mbs"
        cell.value1Label.text = "800"
        cell.title2Label.text = "Speacial Mbs"
        cell.value2Label.text = "250"
        return cell
    }
    
}

extension EasyLoadViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if collectionView == self.operatorsCollectionView{
        return 1
        }
        else {
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if collectionView == self.operatorsCollectionView{
            return 10
        }
        else {
            return 10
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == self.operatorsCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "FavoritesCollectionViewCell", for: indexPath) as! FavoritesCollectionViewCell
            cell.button.setTitle("", for: .normal)
            cell.button.setImage(#imageLiteral(resourceName: "cash.pdf"), for: .normal)
            //TODO: Configure cell
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SelectionButtonCollectioViewCell", for: indexPath) as! SelectionButtonCollectioViewCell
            cell.delegate = self
            return cell
        }
        
    }
}
extension EasyLoadViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.width/3, height: 60)
    }
}

extension EasyLoadViewController: SelectionButtonCellDelegate{
    func typeCellButtonTouched(view: SelectionButtonCollectioViewCell) {
        if let selectedBtn = selectedTypeCell.button {
            selectedTypeCell.unSelectButton()
        }
        //selectedMetaType = typeButton
        selectedTypeCell = view
        view.selectButton()
    }
    
    
}
