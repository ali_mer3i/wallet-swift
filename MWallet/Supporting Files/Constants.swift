//
//  Constants.swift
//  MWallet
//
//  Created by Ali Merhie on 6/27/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import Foundation

//MARK: User Defualts values
let ISLOGGEDIN_DEFUATLTS = "isLoggedIn"
let NAME_DEFUATLTS = "UserName"
let MOBILE_DEFUATLTS = "MobileNumber"
let CURRENT_BALANCE_DEFUATLTS = "CurrentBalance"
let MAIN_WALLET_ID_DEFUATLTS = "MainWalletID"

//MARK: Keychain values

let TOKEN_KEYCHAIN = "Token"

//MARK: Wallet Types
enum WALLET_TYPES_ID: Int {
    case Normal = 1
    case Merchant = 2
    case Dealer = 3
    case Corporate = 4
    case System = 5

}

//MARK: Notification Types
enum NOTIFICATON_TYPES: String {
    case request = "request"
    
}

