//
//  StoryBoardIDs.swift
//  MWallet
//
//  Created by Ali Merhie on 6/18/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import Foundation


let Home_VC = "HomeViewController"
let MoneyTransferMain_VC = "MoneyTransferMainViewController"
let Tickets_VC = "TicketsViewController"
let TabBar_VC = "TabBarCustomViewController"
let LogIn_VC = "LogInViewController"
let LiraAccount_VC = "LiraAccountViewController"
let Contacts_VC = "ContactsViewController"
let EasyLoad_VC = "EasyLoadViewController"
let Register_VC = "RegisterViewController"
let Notification_VC = "NotificationViewController"
let BillPayment_VC = "BillPaymentsViewController"
let QRScanner_VC = "QRScannerViewController"
let QRDisplay_VC = "QRDisplayViewController"




