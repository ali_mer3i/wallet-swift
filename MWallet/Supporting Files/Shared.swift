//
//  Shared.swift
//  Scarlet
//
//  Created by Ali Merhie on 4/30/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import Foundation
import UIKit
import SwiftKeychainWrapper
import SwiftEntryKit

class Shared {
   static var storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    public static func logIn(logInresult: LogInResponseModel) {
        KeychainWrapper.standard.set(logInresult.Token ?? "NoToken", forKey: TOKEN_KEYCHAIN)
        let _: ApiServices = ApiServices()
        //KeychainWrapper.standard.set(logInresult.refresh_token ?? "NoRefreshToken", forKey: "RefreshToken")
        UserDefaults.standard.set(true, forKey: ISLOGGEDIN_DEFUATLTS)
        UserDefaults.standard.set(logInresult.Name, forKey: NAME_DEFUATLTS)
        UserDefaults.standard.set(logInresult.Mobile, forKey: MOBILE_DEFUATLTS)

        navigateWithNavigationBar(viewController: TabBar_VC)

    }
    @objc public static func logOut() {
        KeychainWrapper.standard.removeObject(forKey: TOKEN_KEYCHAIN)
        UserDefaults.standard.set(false, forKey: ISLOGGEDIN_DEFUATLTS)
        UserDefaults.standard.set("", forKey: NAME_DEFUATLTS)
        UserDefaults.standard.set("", forKey: MOBILE_DEFUATLTS)
        
        navigateWithNavigationBar(viewController:  LogIn_VC)
        //Shared.displayAlert(title: "Session Expired", message: "You will be logged out", cancelText: "Dismiss")
    }
    
    @objc public static func openNotifications() {
        
      //navigateWithNavigationBar(viewController: Notification_VC)
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let loginPageView = mainStoryboard.instantiateViewController(withIdentifier: Notification_VC) as! NotificationViewController
        var rootViewController = AppDelegate.shared.window!.rootViewController as! UINavigationController
        rootViewController.pushViewController(loginPageView, animated: true)
        
    }
    
    @objc public static func clickedLogOut() {
        displayConfirmAlert(title: "Logout", message: "Are you sure you want to logout?" , confirmText: "Yes", cancelText: "No") { (isconfirmed) in
            if isconfirmed {
                logOut()
            }
        }
    }
    public static func displayConfirmAlert(title: String, message: String, confirmText: String, cancelText: String, completion: @escaping (Bool) -> () ) {
        let vc = AppDelegate.shared.window?.rootViewController
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: cancelText, style: .cancel,  handler: { (action) in completion(false) })
        let confirm = UIAlertAction(title: confirmText, style: .destructive, handler: { (action) in completion(true) })
        
        alert.addAction(cancel)
        alert.addAction(confirm)
        
        vc?.present(alert, animated: true, completion: nil)
    }
    public static func displayAlert(title: String, message: String, cancelText: String) {
        let vc = AppDelegate.shared.window?.rootViewController
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let cancel = UIAlertAction(title: cancelText, style: .cancel, handler: nil)
        alert.addAction(cancel)
        vc?.present(alert, animated: true, completion: nil)
    }
    public static func navigateWithNavigationBar(viewController: String){
        let nextViewController = self.storyboard.instantiateViewController(withIdentifier: viewController)

        //add setting button to navigation
        if viewController != LogIn_VC && viewController != Register_VC {
            let buttonSetting = UIBarButtonItem(image: UIImage(named: "settings"), style: .plain, target: self, action: #selector(self.clickedLogOut))
            let buttonNotification = UIBarButtonItem(image: UIImage(named: "notification"), style: .plain, target: self, action: #selector(self.openNotifications))

            nextViewController.navigationItem.rightBarButtonItems  = [buttonSetting, buttonNotification]
        }

        let nav = UINavigationController(rootViewController: nextViewController)
        nav.navigationController?.navigationBar.prefersLargeTitles = true
        nav.navigationItem.largeTitleDisplayMode = .always


        AppDelegate.shared.window!.rootViewController = nav
    }
    
    public static func pushVC(rootView: UIViewController, viewController: String){
        
        let nextViewController = (self.storyboard.instantiateViewController(withIdentifier: viewController))
        rootView.navigationController?.pushViewController(nextViewController, animated: true)

    }


    }


