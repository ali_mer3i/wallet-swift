//
//  QRDisplayViewController.swift
//  MWallet
//
//  Created by Ali Merhie on 7/9/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import UIKit

class QRDisplayViewController: UIViewController {
    var qrcodeImage: CIImage!
    @IBOutlet weak var scanQRButton: UIButton!
    @IBOutlet weak var QRImageSectionView: UIView!
    @IBOutlet weak var scanSectionView: UIView!
    @IBOutlet weak var ExplainDetailsSectionView: UIView!
    @IBOutlet weak var QRCodeImage: UIImageView!
    @IBOutlet weak var circleImageBackView: UIView!
    @IBOutlet weak var walletIDLabel: UILabel!
    @IBOutlet weak var explainLabel: UILabel!
    
    @IBOutlet weak var scannerView: QRScannerView!{
        didSet {
            scannerView.delegate = self
        }
    }    
    
    var qrData: QRData? = nil {
        didSet {
            if qrData != nil {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let transferMonyView = mainStoryboard.instantiateViewController(withIdentifier: LiraAccount_VC) as! LiraAccountViewController
                transferMonyView.scannedPhoneNumber = self.qrData?.codeString
                var rootViewController = AppDelegate.shared.window!.rootViewController as! UINavigationController
                rootViewController.pushViewController(transferMonyView, animated: true)
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        scanSectionView.isHidden = true
       explainLabel.text = "Scan the code on your contact's phone, or ask them to scan your code, to trasnfer money from or to their wallets. You can also directly open transfer page. This is optional"
        walletIDLabel.text = UserDefaults.standard.string(forKey: MOBILE_DEFUATLTS)
        let gestureQRView = UITapGestureRecognizer(target: self, action:  #selector(self.closeScanerCamera))
        self.QRImageSectionView.addGestureRecognizer(gestureQRView)

        if qrcodeImage == nil {
            let number = UserDefaults.standard.string(forKey: MOBILE_DEFUATLTS) ?? ""
            let data = number.data(using: String.Encoding.isoLatin1, allowLossyConversion: false)
            
            let filter = CIFilter(name: "CIQRCodeGenerator")!
            
            filter.setValue(data, forKey: "inputMessage")
            filter.setValue("Q", forKey: "inputCorrectionLevel")
            
            qrcodeImage = filter.outputImage
            
            displayQRCodeImage()

        }
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !scanSectionView.isHidden {
            if !scannerView.isRunning {
                scannerView.startScanning()
            }
        }
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        circleImageBackView.layer.cornerRadius = circleImageBackView.frame.size.width/2
        circleImageBackView.clipsToBounds = true
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !scannerView.isRunning {
            scannerView.stopScanning()
        }
    }
    @objc func closeScanerCamera(sender : UITapGestureRecognizer) {
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.scanSectionView.isHidden = true
            self.ExplainDetailsSectionView.isHidden = false
            
        })
        if !scannerView.isRunning {
            scannerView.stopScanning()
        }
    }

    func displayQRCodeImage() {
        let scaleX = QRCodeImage.frame.size.width / qrcodeImage.extent.size.width
        let scaleY = QRCodeImage.frame.size.height / qrcodeImage.extent.size.height
        
        let transformedImage = qrcodeImage.transformed(by: CGAffineTransform(scaleX: scaleX, y: scaleY))
        
        QRCodeImage.image = UIImage(ciImage: transformedImage)
        
    }
    
    @IBAction func openScanQRClicked(_ sender: Any) {
        
        UIView.transition(with: view, duration: 0.5, options: .transitionCrossDissolve, animations: {
            self.scanSectionView.isHidden = false
            self.ExplainDetailsSectionView.isHidden = true
            
        })
        
        
        if !scannerView.isRunning {
            scannerView.startScanning()
        }
    }
    
}

extension QRDisplayViewController: QRScannerViewDelegate {
    func qrScanningDidStop() {
        let buttonTitle = scannerView.isRunning ? "STOP" : "SCAN"
       // scanButton.setTitle(buttonTitle, for: .normal)
    }
    
    func qrScanningDidFail() {
        Shared.displayAlert(title: "Error", message: "Scanning Failed. Please try again", cancelText: "Dismiss")
    }
    
    func qrScanningSucceededWithCode(_ str: String?) {
        self.qrData = QRData(codeString: str)
    }
}
