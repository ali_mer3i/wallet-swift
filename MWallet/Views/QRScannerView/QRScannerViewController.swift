//
//  QRScannerViewController.swift
//  MWallet
//
//  Created by Ali Merhie on 7/9/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//


import UIKit

class QRScannerViewController: UIViewController {
    
    @IBOutlet weak var scannerView: QRScannerView!{
        didSet {
            scannerView.delegate = self
        }
    }
    
    @IBOutlet weak var scanButton: UIButton!{
        didSet {
            scanButton.setTitle("STOP", for: .normal)
        }
    }
    
    var qrData: QRData? = nil {
        didSet {
            if qrData != nil {
                let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let transferMonyView = mainStoryboard.instantiateViewController(withIdentifier: LiraAccount_VC) as! LiraAccountViewController
                transferMonyView.scannedPhoneNumber = self.qrData?.codeString
                var rootViewController = AppDelegate.shared.window!.rootViewController as! UINavigationController
                rootViewController.pushViewController(transferMonyView, animated: true)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if !scannerView.isRunning {
            scannerView.startScanning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if !scannerView.isRunning {
            scannerView.stopScanning()
        }
    }
    
    @IBAction func scanButtonAction(_ sender: UIButton) {
        scannerView.isRunning ? scannerView.stopScanning() : scannerView.startScanning()
        let buttonTitle = scannerView.isRunning ? "STOP" : "SCAN"
        sender.setTitle(buttonTitle, for: .normal)
    }
    
    @IBAction func myQRClicked(_ sender: Any) {
        Shared.pushVC(rootView: self, viewController: QRDisplay_VC)
    }
}


extension QRScannerViewController: QRScannerViewDelegate {
    func qrScanningDidStop() {
        let buttonTitle = scannerView.isRunning ? "STOP" : "SCAN"
        scanButton.setTitle(buttonTitle, for: .normal)
    }
    
    func qrScanningDidFail() {
        Shared.displayAlert(title: "Error", message: "Scanning Failed. Please try again", cancelText: "Dismiss")
    }
    
    func qrScanningSucceededWithCode(_ str: String?) {
        self.qrData = QRData(codeString: str)
    }
}


extension QRScannerViewController {
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        if segue.identifier == "detailSeuge", let viewController = segue.destination as? DetailViewController {
//            viewController.qrData = self.qrData
//        }
     
    }
}

