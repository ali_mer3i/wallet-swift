//
//  LinedTextFieldView.swift
//  MWallet
//
//  Created by Ali Merhie on 6/20/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import UIKit

class LinedTextFieldView: UIView {
    @IBOutlet var view: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var line1View: UIView!
    @IBOutlet weak var line2View: UIView!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setUp()
    }
    init() {
        super.init(frame: .infinite)
        setUp()
    }
    
    private func setUp(){
        Bundle.main.loadNibNamed("LinedTextFieldView", owner: self, options: nil)
        
        self.addSubview(view)
        addConstraints()
        
        errorLabel.isHidden = true
    }
    
    public func displayError(error: String){
        errorLabel.text = error
        errorLabel.isHidden = false
        titleLabel.textColor = #colorLiteral(red: 0.8588235294, green: 0, blue: 0, alpha: 1)
        line1View.backgroundColor = #colorLiteral(red: 0.8588235294, green: 0, blue: 0, alpha: 1)
        line2View.backgroundColor = #colorLiteral(red: 0.8588235294, green: 0, blue: 0, alpha: 1)
        textField.borderColor = #colorLiteral(red: 0.8588235294, green: 0, blue: 0, alpha: 1)
    }
    public func resetView(){
        errorLabel.text = ""
        errorLabel.isHidden = true
        titleLabel.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        line1View.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        line2View.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        textField.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }
    func addConstraints() {
        view.translatesAutoresizingMaskIntoConstraints = false
        var anchors = [NSLayoutConstraint]()
        anchors.append(topAnchor.constraint(equalTo: view.topAnchor, constant: 0))
        anchors.append(bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0))
        anchors.append(leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0))
        anchors.append(trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0))
        NSLayoutConstraint.activate(anchors)
        
    }


}
