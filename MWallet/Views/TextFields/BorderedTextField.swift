//
//  BorderedTextField.swift
//  MWallet
//
//  Created by Ali Merhie on 6/14/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import UIKit

class BorderedTextField: UITextField {
    
    
    //initWithFrame to init view from code
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    //initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    //common func to init our view
    private func setupView() {
        borderStyle = .none
        layer.borderWidth = 1
        layer.borderColor = #colorLiteral(red: 0.5568627451, green: 0.5568627451, blue: 0.5764705882, alpha: 1)
        layer.cornerRadius = 10
        textAlignment = .center
        
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
