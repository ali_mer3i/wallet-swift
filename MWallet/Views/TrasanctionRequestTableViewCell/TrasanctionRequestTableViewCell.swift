//
//  TrasanctionRequestTableViewCell.swift
//  MWallet
//
//  Created by Ali Merhie on 7/2/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import UIKit

protocol TrasactionCellDelegate: class {
    // you can add parameters if you want to pass. something to controller
    func responseButtonClicked(isAccepted: Bool, trasanctionID: String)
}


class TrasanctionRequestTableViewCell: UITableViewCell {
    public weak var delegate: TrasactionCellDelegate?

    var trasanctionID: String = ""
    @IBOutlet weak var buttonsView: UIStackView!
    @IBOutlet weak var rejectButton: UIButton!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        acceptButton.setTitle("Accept", for: .normal)
        rejectButton.setTitle("Decline", for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func responseClicked(_ sender: UIButton) {
        var isAccepted = true
        if sender.tag == 0 {
           isAccepted = false
        }
        
         delegate?.responseButtonClicked(isAccepted: isAccepted, trasanctionID: trasanctionID)
    }
}
