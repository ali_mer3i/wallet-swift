//
//  BorderedButton.swift
//  MWallet
//
//  Created by Ali Merhie on 6/14/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import UIKit

class BorderedButton: UIButton {
    
    //initWithFrame to init view from code
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    //initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    //common func to init our view
    private func setupView() {
        layer.borderWidth = 1
        layer.borderColor = #colorLiteral(red: 0.5333333333, green: 0.7019607843, blue: 0.2156862745, alpha: 1)
        layer.cornerRadius = 10
        setTitleColor(#colorLiteral(red: 0.5333333333, green: 0.7019607843, blue: 0.2156862745, alpha: 1) , for: .normal)
        titleLabel?.font = UIFont(name: "ProximaNova-Bold", size: 17)
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
