//
//  FilledButton.swift
//  MWallet
//
//  Created by Ali Merhie on 6/14/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import UIKit

class FilledButton: UIButton {
    
    //initWithFrame to init view from code
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    //initWithCode to init view from xib or storyboard
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }
    
    //common func to init our view
    private func setupView() {
        backgroundColor = #colorLiteral(red: 0.5333333333, green: 0.7019607843, blue: 0.2156862745, alpha: 1)
        setTitleColor(UIColor.white, for: .normal)
        layer.cornerRadius = 10
        
        
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
