//
//  BoxView.swift
//  MWallet
//
//  Created by Ali Merhie on 6/17/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import UIKit

class BoxView: UIView {
    @IBOutlet var view: UIView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleText: UILabel!
    @IBOutlet weak var descriptionText: UILabel!
    
    public var currentViewController : UIViewController?
    public var nextVC : String?

    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        Bundle.main.loadNibNamed("BoxView", owner: self, options: nil)
        
        self.addSubview(view)
        addConstraints()
    }
    init() {
        super.init(frame: .infinite)
        Bundle.main.loadNibNamed("BoxView", owner: self, options: nil)
        
        self.addSubview(view)
        addConstraints()
        
    }
    func addConstraints() {
        view.translatesAutoresizingMaskIntoConstraints = false
        var anchors = [NSLayoutConstraint]()
        anchors.append(topAnchor.constraint(equalTo: view.topAnchor, constant: 0))
        anchors.append(bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0))
        anchors.append(leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0))
        anchors.append(trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0))
        NSLayoutConstraint.activate(anchors)
        
    }
    @IBAction func ViewClicked(_ sender: Any) {
        if let VC = currentViewController {
        let secondViewController = VC.storyboard?.instantiateViewController(withIdentifier: nextVC ?? "")
        VC.navigationController?.pushViewController(secondViewController!, animated: true)
        }
        
    }
    
}
