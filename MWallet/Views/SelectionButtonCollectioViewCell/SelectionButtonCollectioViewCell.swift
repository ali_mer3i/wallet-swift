//
//  FavoritesCollectionViewCell.swift
//  MWallet
//
//  Created by Ali Merhie on 6/21/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import UIKit

protocol SelectionButtonCellDelegate: class {
    // you can add parameters if you want to pass. something to controller
    func typeCellButtonTouched(view: SelectionButtonCollectioViewCell)
}


class SelectionButtonCollectioViewCell: UICollectionViewCell {
    //delegate for handling button clicked
    public weak var delegate: SelectionButtonCellDelegate?
    
    @IBOutlet weak var button: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        let leftConstraint = contentView.leftAnchor.constraint(equalTo: leftAnchor)
        let rightConstraint = contentView.rightAnchor.constraint(equalTo: rightAnchor)
        let topConstraint = contentView.topAnchor.constraint(equalTo: topAnchor)
        let bottomConstraint = contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
        NSLayoutConstraint.activate([leftConstraint, rightConstraint, topConstraint, bottomConstraint])
        
    }
    
    public func selectButton(){
        button.setTitleColor(UIColor.white, for: .normal)
        button.backgroundColor = #colorLiteral(red: 0.5333333333, green: 0.7019607843, blue: 0.2156862745, alpha: 1)
    }
    
    public func unSelectButton(){
        button.setTitleColor(UIColor.black, for: .normal)
        button.backgroundColor = #colorLiteral(red: 0.9098039216, green: 0.9098039216, blue: 0.9098039216, alpha: 1)
    }
    @IBAction func buttonClicked(_ sender: Any) {
        delegate?.typeCellButtonTouched(view: self)
    }
}
