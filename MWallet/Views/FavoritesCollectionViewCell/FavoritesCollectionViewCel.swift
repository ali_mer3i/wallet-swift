//
//  FavoritesCollectionViewCell.swift
//  MWallet
//
//  Created by Ali Merhie on 6/21/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import UIKit

protocol FavoriteButtonCellDelegate: class {
    // you can add parameters if you want to pass. something to controller
    func FavoriteContactSelected(contact: ContactsFrequentCore)
}

class FavoritesCollectionViewCell: UICollectionViewCell {
    public weak var delegate: FavoriteButtonCellDelegate?

    @IBOutlet weak var button: UIButton!
    var contact : ContactsFrequentCore?
    
    @IBAction func buttonClicked(_ sender: Any) {
        delegate?.FavoriteContactSelected(contact: self.contact!)
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        let leftConstraint = contentView.leftAnchor.constraint(equalTo: leftAnchor)
        let rightConstraint = contentView.rightAnchor.constraint(equalTo: rightAnchor)
        let topConstraint = contentView.topAnchor.constraint(equalTo: topAnchor)
        let bottomConstraint = contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
        NSLayoutConstraint.activate([leftConstraint, rightConstraint, topConstraint, bottomConstraint])
        
    }
}
