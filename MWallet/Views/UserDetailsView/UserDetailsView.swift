//
//  UserDetailsView.swift
//  MWallet
//
//  Created by Ali Merhie on 6/18/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import UIKit

class UserDetailsView: UIView {
    let apiservices: ApiServices = ApiServices()

    @IBOutlet var view: UIView!
    @IBOutlet weak var nameText: UILabel!
    @IBOutlet weak var numberText: UILabel!
    @IBOutlet weak var balanceText: UILabel!
    @IBOutlet weak var balanceValueText: UILabel!
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        Bundle.main.loadNibNamed("UserDetailsView", owner: self, options: nil)
        
        self.addSubview(view)
        addConstraints()
    }
    
    override func awakeFromNib() {
        self.nameText.text = UserDefaults.standard.string(forKey: NAME_DEFUATLTS)
        self.numberText.text =  UserDefaults.standard.string(forKey: MOBILE_DEFUATLTS)
        self.balanceText.text = "Current Balance"
        loadData()
    }
    
    public func loadData(){
        self.balanceValueText.text =  UserDefaults.standard.string(forKey: CURRENT_BALANCE_DEFUATLTS) ?? "0"
        
        ApiServices.getAllWallets { (walletsResponse, statusCode) in
            if statusCode == 200{
                let count = walletsResponse?.count
                if count! > 0 {
                    let wallet = walletsResponse![0]
                    UserDefaults.standard.set(wallet.id, forKey: MAIN_WALLET_ID_DEFUATLTS)
                    UserDefaults.standard.set(wallet.balance, forKey: CURRENT_BALANCE_DEFUATLTS)
                    self.balanceValueText.text =  wallet.balance ?? "0"
                    
                }
            }
        }
    }
    
    init() {
        super.init(frame: .infinite)
        Bundle.main.loadNibNamed("UserDetailsView", owner: self, options: nil)
        
        self.addSubview(view)
        addConstraints()
  

        
    }
    func addConstraints() {
        view.translatesAutoresizingMaskIntoConstraints = false
        var anchors = [NSLayoutConstraint]()
        anchors.append(topAnchor.constraint(equalTo: view.topAnchor, constant: 0))
        anchors.append(bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0))
        anchors.append(leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0))
        anchors.append(trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0))
        NSLayoutConstraint.activate(anchors)
        
    }

}
