//
//  BundelsTableViewCell.swift
//  MWallet
//
//  Created by Ali Merhie on 6/21/19.
//  Copyright © 2019 Monty Mobile. All rights reserved.
//

import UIKit

class BundelsTableViewCell: UITableViewCell {

    @IBOutlet weak var button: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var validityLabel: UILabel!
    @IBOutlet weak var title1Label: UILabel!
    @IBOutlet weak var value1Label: UILabel!
    @IBOutlet weak var title2Label: UILabel!
    @IBOutlet weak var value2Label: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        button.setTitle("Activate", for: .normal)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
